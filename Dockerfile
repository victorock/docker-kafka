# Raisonata Docker:
#
# VERSION               0.1
#
# Kafka with Zookeeper and supervisor

FROM raisonata/java:oracle-java8

MAINTAINER Victor da Costa <victorockeiro@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

#ENV KAFKA_EXTERNAL_HOST
#ENV KAFKA_EXTERNAL_PORT
ENV KAFKA_HOME /opt/kafka_2.8.0-0.8.1.1
ENV ZK_CHROOT kafka

# Install Kafka, Zookeeper and other needed things
RUN apt-get update && \
    apt-get install -y zookeeper wget supervisor dnsutils && \
    rm -rf /var/lib/apt/lists/* && \
    wget -q http://mirror.gopotato.co.uk/apache/kafka/0.8.1.1/kafka_2.8.0-0.8.1.1.tgz -O /tmp/kafka_2.8.0-0.8.1.1.tgz && \
    tar xfz /tmp/kafka_2.8.0-0.8.1.1.tgz -C /opt && \
    rm /tmp/kafka_2.8.0-0.8.1.1.tgz

ADD scripts/start-all.sh /usr/local/bin/start-all
ADD scripts/start-kafka.sh /usr/local/bin/start-kafka

# Supervisor config
ADD conf/kafka.conf /etc/supervisor/conf.d/kafka.conf
ADD conf/zookeeper.conf /etc/supervisor/conf.d/zookeeper.conf

# 2181 is zookeeper, 9092 is kafka
EXPOSE 2181 9092

# Let's launch our script to check configuration before starting supervisord
ENTRYPOINT ["start-all"]
CMD [""]
