#!/bin/bash

# Optional ENV variables:
# * HELIOS_PORT_kafka: the external hostname and port, e.g. "awseu3-heliosciagent-a1.spotify.net:9092"
# * ZK_CHROOT: the zookeeper chroot that's used by Kafka (without / prefix), e.g. "kafka"

ZK_CHROOT=${ZK_CHROOT:-"kafka"}
KAFKA_EXTERNAL_HOST=${KAFKA_EXTERNAL_HOST?'You need to specify our external KAFKA_EXTERNAL_HOST'}
KAFKA_EXTERNAL_PORT=${KAFKA_EXTERNAL_PORT?'You need to specify our external KAFKA_EXTERNAL_PORT'}

sed -r -i "s/#(advertised.host.name)=(.*)/\1=$KAFKA_EXTERNAL_HOST/g" $KAFKA_HOME/config/server.properties
sed -r -i "s/#(advertised.port)=(.*)/\1=$KAFKA_EXTERNAL_PORT/g" $KAFKA_HOME/config/server.properties

# Set the zookeeper chroot
until /usr/share/zookeeper/bin/zkServer.sh status; do
	sleep 0.1
done

# create the chroot node
echo "create /$ZK_CHROOT \"\"" | /usr/share/zookeeper/bin/zkCli.sh || {
    echo "can't create chroot in zookeeper, exit"
    exit 1
}

# configure kafka
sed -r -i "s/(zookeeper.connect)=(.*)/\1=localhost:2181\/$ZK_CHROOT/g" $KAFKA_HOME/config/server.properties

supervisord -n